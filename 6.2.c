#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int next_number(int current)
{
	if ((current % 2) == 0)
		return current / 2;
	else
		return 3 * current + 1;
}


int main() {
	int n, next, maxcount_n;
	int count, maxcount = 0;
	printf("\n");
	for (n = 2; n <= 1000000; ++n)
	{
		printf("%d              \r", n);
		count = 0;
		next = n;
		while (next > 1)
		{
			next = next_number(next);
			++count;
		}
		if (count > maxcount)
		{
			maxcount = count;
			maxcount_n = n;
		}
	}
	printf("\nn = %d, max count = %d\n", maxcount_n, maxcount);
	return 0;
}