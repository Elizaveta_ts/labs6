/*���������, ������� �������� ����� ���������� N-���
����� ���� ���������.������� ��������� �� ����� � ���������� � ���� */
#include<stdio.h>
#include<stdlib.h>
#include<time.h>


unsigned long int foo(unsigned int n)
{
	if (n == 0)
		return 0;
	if (n == 2 || n == 1)
		return 1;
	else
		return foo(n - 1) + foo(n - 2);
}


int main()
{
	FILE *file;
	clock_t a, b;
	int i, n;
	unsigned long int result;
	file = fopen("1.txt", "w");
	if (file == 0)
	{
		perror("File error: ");
		return 1;
	}
	printf("Print number of sequence");
	scanf("%d", &n);

	for (i = 1; i <= n; i++)
	{
		a = clock(); //������ ������ ���������
		result = foo(i);
		b = clock(); //����� ������ ���������
		printf("number: %d | result: %d | time: %.2f\n", i,result, (double)(b - a)/CLK_TCK);
		fprintf(file, "number: %d | result: %d | time: %.2f\n", i, result, (double)(b - a)/CLK_TCK);

	}
	fclose(file);
	return 0;
}