#include <stdio.h>

#define SIZE 200

int power(int k) //���������� ������ � ������� �-1
{
	int i;
	int val = 1;
	for (i = 0; i < k; i++)
		val *= 3;
	return val;
}
char canvas[SIZE][SIZE];

void drawFractal(int x, int y, int k)
{
	if (k == 0)
		canvas[y][x] = '*';
	else
	{
		drawFractal(x, y-power(k-1), k - 1);
		drawFractal(x + power(k - 1), y, k - 1);
		drawFractal(x, y + power(k - 1), k - 1);
		drawFractal(x - power(k - 1), y, k - 1);
		drawFractal(x, y, k - 1);
	}
}

void clearCanvas()
{
	int i, j;
	for (i = 0; i < SIZE; i++)
		for (j = 0; j< SIZE; j++)
		{
			canvas[i][j] = ' ';
		}
}

void showFractal()
{
	int i, j = 0;
	for (i = 0; i < SIZE; i++)
		for (j = 0; j < SIZE; j++)
			putchar(canvas[i][j]);
	putchar('\n');
}

int main()
{
	clearCanvas();
	drawFractal(SIZE / 2, SIZE / 2, 4);
	showFractal();
return 0;
}
