/*���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������
*/
#include <string.h>
#include <stdio.h>


int to_string(int n, char *buf)
{ 
	int flag = 1;
	int i = 0;
	
	if(buf[0] == '-')
	{
		buf++;
		n--;
		flag = -1;
	}


	if (n > 0)
			return (buf[n] - '0' + 10 * to_string(n - 1, buf)) * flag;
	else 
	{	
		return (buf[n] - '0') * flag;
	}
}



int main()
{
	int buf[100];
	int result;
	printf("Input a valid string to convert to integer\n");
	scanf("%s",buf);
	result = to_string(strlen(buf)-1, buf);
	printf("String  = %s\nInteger = %d\n",buf, result);
	return 0;
}